'use strict'
import {textAnalysis} from './textanalysis.js';
import {$, init} from './main.js';
import {text3} from './testspecial.js';
import {hayStack} from './haystack.js';
import {text1, text2} from './ebook.js';

/**https://flexiple.com/loop-through-object-javascript/ */
    function domainUrl(){
    let userInput = document.getElementById('inputUrl').value;
    console.log(userInput + "");
    let userResult = $('resultUrl');
    /**Regular expression regex: help found on: https://regex101.com/ and https://www.debuggex.com/ */
    let regex = /^(http:\/\/\www.|http:\/\/|www.|https:\/\/\www.)(\w*\.\w*)/;
    let s = userInput.match(regex);
    userResult.innerHTML = s[2];

    }
    function domainUrlLink(){
    let userResult = $('resultUrlLink');
    let regex = /<a\s*href=[\'"](.+?)[\'"].*?>(.+)<\/a>/gi;;
    let s = hayStack.match(regex);
    // console.log(s);
    s.forEach(item => {
        userResult.innerHTML += " " + item;
    });
    
    }
    function countWords(){
    let resultCount = $('resultNumbers');
    let re = /\b[a-z]+\b/gi;
    let arr = new textAnalysis(text2);
    let wordlist = arr.createWordList(re);
    resultCount.innerHTML = "Number of words are: " + wordlist.length;
  
    }
    function countWordsWithoutDuplicate(){
    let resultCount = $('resultWord');
    let arr = new textAnalysis(text2);
    let regex = /\b[a-z]+\b/gi;
    let array = [];


    let wordList = arr.createWordList(regex);
    // loop through array
    for(let i of wordList) {
    if(array.indexOf(i) === -1) {
        array.push(i);
    }
    }

// console.log(uniqueArr);
    resultCount.innerHTML = "Number of words without duplicate: " + array.length;
   
    
    }

    function countSingleWords(){
    let arr = new textAnalysis(text2);
    let regex = /[a-z]/gi;
    let letters = arr.createWordList(regex);

    let resultOutcome = $('resultEnglishLetterOutcome');
    /**Sorting array */
    letters.sort();
    // console.log(letters);
    let total = letters.length;
     /** making all letters to lowerscase. */
    let lettersLowerCase = letters.map(item => item.toLowerCase());
  
    let obj = arr.wordLetterFrequencies(lettersLowerCase);

    let histrogramResult = arr.wordLetterHistrogram(obj, total);
    let tableResult = arr.wordLetterCount(obj, total);
    resultOutcome.innerHTML += " This displays a histrogram of the outcome. " + "<br>" + histrogramResult;
    resultOutcome.innerHTML += " " + tableResult;
     
    }

    function countDanishWord(){
    let arr = new textAnalysis(text1);
    let resultCount = $('resultWordDanishCount');
    let re = /\b[a-zA-Z0-9æøåÆØÅ]+\b/gi;
    let wordList = arr.createWordList(re);
    resultCount.innerHTML = "Number of Danish words:  " + wordList.length;
    
    }
    function countDansihWordsWithoutDuplicate(){
    let resultCount = $('resultWordDanishWithoutDuplicate');
    let arr = new textAnalysis(text1);
    let re = /[a-zA-Z0-9æøåÆØÅ]+/gi;
    let array = [];

    let wordList = arr.createWordList(re);
    // loop through array
    for(let i of wordList) {
    if(array.indexOf(i) === -1) {
        array.push(i);
    }
}
resultCount.innerHTML = "Number of words without duplicate: " + array.length;
    }
    function countDanishSingleWords(){
    let arr = new textAnalysis(text1);
    let regex = /[a-zæøå]/gi;
    let letters = arr.createWordList(regex);
    let resultOutcome = $('countDanishLetter');
    letters.sort();
    let total = letters.length;
    /** making all letters to lowerscase. */
    let lettersLowerCase = letters.map(item => item.toLowerCase());
    const objDanish = arr.wordLetterFrequencies(lettersLowerCase);
    /** Store items in our object called obj */
    let histrogramResult = arr.wordLetterHistrogram(objDanish, total);
    let tableResult = arr.wordLetterCount(objDanish, total);
    resultOutcome.innerHTML += " This displays a histrogram of the outcome. " + "<br>" + histrogramResult; + histrogramResult;
    resultOutcome.innerHTML += " " + tableResult;
        
    }
    function countDanishCharacters(){
        let resultCount = $('resultTestCharacters');
        let arr = new textAnalysis(text3);
        /** can´t get æå to display so it´s not part of regex. another one is /\b(^[a-zA-Z0-9a-æøå].+)\b/gi but it dosen´t work if  you type write ærø.(se billedet). \b[a-zæøå].+\w\b */
        // let regex = /^[\wæøåÆØÅ]+$/gi;
        let regex = /\b(^[a-zA-Z0-9a-æøå].+)\b/gi;
        // let regex = new RegExp();
        // let regex = /\D[a-zA-Z0-9ÆØÅæøå!"#$%&'()*+,-.:;<=>?@[\]^_`{|}~]+\D/gi;
        let wordList = arr.createWordList(regex);
        wordList.forEach(item => {
            resultCount.innerHTML += " " + item;
        });
            resultCount.innerHTML += " all special characters can be used within regex. "
        }
    

    const doSomething = function () {
    init();
    countWords();
    countWordsWithoutDuplicate();
    domainUrlLink();
    countDanishCharacters();
    countSingleWords();
    countDanishWord();
    countDansihWordsWithoutDuplicate();
    countDanishSingleWords();
    $('urlButton').addEventListener('click', domainUrl);
    }
    window.addEventListener("load", doSomething);