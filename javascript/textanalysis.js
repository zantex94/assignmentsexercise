'use strict'
// text1 is Danish and text2 are English one.
// import {text1, text2} from './ebook';


export class textAnalysis{
    constructor(s){
        this.s = s;
    }

createWordList(x){
    let wordList = this.s.match(x);
    return wordList;
}
wordLetterFrequencies(x){
       // /**creating an object */
    const obj = {};
      // /** Store items in our object called obj */
    x.forEach(item => {
        if(obj[item]){
        obj[item]++;
        }else{
        obj[item] = 1;
        }
    });
    return obj;
}
wordLetterHistrogram(obj, total){
    let array = [];
    // for (const [key, value] of Object.entries(obj)) {
        Object.entries(obj).forEach(([key, value]) => {
        let h = ((value / total) * 100).toFixed(1);
        array += key + " : ";
        while(h > 0 && h >= 0.5 ){
                    array += "*  ";
                    h--;
                }
                array += "<br>";
});
return array;

}

wordLetterCount(obj, total){
    let s = '<table>';
    s += '<tr><th>Character</th><th>Value count</th><th>%</th></tr>';
    Object.entries(obj).forEach(([key, value]) => {
        obj[value] = Number((value / total) * 100).toFixed(1) + "%";
        s += `<tr><td>${key}</td><td>${[value]}</td><td>${obj[value]}</td></tr>`;
    });
    s += '</table>';
    return s;

}
}