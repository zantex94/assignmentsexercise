'use strict'
import {$, init} from './main.js';

let buildSvg = function() {
    let xmlns = 'http://www.w3.org/2000/svg';
    let svg = document.createElementNS(xmlns, 'svg');
    svg.setAttributeNS(null, 'viewbox', '0 0 400 200');
    svg.setAttributeNS(null, 'preserveAspectRatio', 'none');
    svg.setAttributeNS(null, 'class', 'svg0');
    svg.setAttributeNS(null, 'style', 'min-height:400px; min-width:800px; background-color:black');
    var scaleCoefficient = 1; //this will make value times bigger in x/y direction
    svg.setAttribute("transform", "scale(" + scaleCoefficient + "," + scaleCoefficient + ")");
    let p = document.createElementNS(xmlns, 'ellipse');
    p.setAttributeNS(null, 'cx', 400);
    p.setAttributeNS(null, 'cy', 200);
    p.setAttributeNS(null, 'rx', 360);
    p.setAttributeNS(null, 'ry', 160);
    p.setAttributeNS(null, 'r', 10);
    p.setAttributeNS(null, 'stroke', 'blue');
    p.setAttributeNS(null, 'stroke-width', 2);
    p.setAttributeNS(null, 'fill', 'transparent');
    svg.appendChild(p);
    let c = document.createElementNS(xmlns, 'circle');
    c.setAttributeNS(null, 'cx', 400);
    c.setAttributeNS(null, 'cy', 200);
    c.setAttributeNS(null, 'r', 10);
    c.setAttributeNS(null, 'fill', 'yellow');
    svg.appendChild(c);
    $('svg0').appendChild(svg);
}

const doSvg = function () {
    init();
    buildSvg();
}
window.addEventListener('load', doSvg);